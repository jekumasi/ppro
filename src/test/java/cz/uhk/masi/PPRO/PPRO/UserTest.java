package cz.uhk.masi.PPRO.PPRO;

import cz.uhk.masi.PPRO.PPRO.dao.CustomerRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorUserRepository;
import cz.uhk.masi.PPRO.PPRO.model.Customer;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.JanitorUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTest {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private JanitorRepository janitorRepository;

    @Test
    public void customerFindByName(){

        Customer customer = new Customer();
        customer.setName("customer@email.cz");

        Customer customerFromDB = customerRepository.findByName(customer.getName()).get(0);

        assertThat(customerFromDB.getName()).isEqualTo(customer.getName());

    }

    @Test
    public void janitorFindByEmail(){

        Janitor janitor = new Janitor();
        janitor.setEmail("test@example.com");

        Janitor janitorFromDB = janitorRepository.findByEmail(janitor.getEmail()).get(0);

        assertThat(janitorFromDB.getEmail()).isEqualTo(janitor.getEmail());

    }

}
