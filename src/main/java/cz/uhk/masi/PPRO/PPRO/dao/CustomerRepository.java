package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.Customer;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> findByName(String name);
}
