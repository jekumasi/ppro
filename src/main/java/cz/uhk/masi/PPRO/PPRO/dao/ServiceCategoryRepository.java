package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.ServiceCategory;
import org.springframework.data.repository.CrudRepository;

public interface ServiceCategoryRepository extends CrudRepository<ServiceCategory, Integer> {
}
