package cz.uhk.masi.PPRO.PPRO.controller.admin;

import cz.uhk.masi.PPRO.PPRO.dao.JanitorRepository;
import cz.uhk.masi.PPRO.PPRO.dao.ServiceCategoryRepository;
import cz.uhk.masi.PPRO.PPRO.dao.ServiceRepository;
import cz.uhk.masi.PPRO.PPRO.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class ServiceController {
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    ServiceCategoryRepository serviceCategoryRepository;
    @Autowired
    JanitorRepository janitorRepository;
    @RequestMapping("/admin/janitor/service")
    public String adminReview(Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        Janitor j = janitorRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);
        model.addAttribute("services", serviceRepository.findByJanitor(j));
        return "admin/janitor/service/service";

    }
    @RequestMapping("/admin/janitor/service/create")
    public String createJanitorService(Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        Janitor j = janitorRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);
        Service s = new Service();
        s.setJanitor(j);
        model.addAttribute("service",s);
        model.addAttribute("serviceCategories", serviceCategoryRepository.findAll());
        return "admin/janitor/service/create";
    }
    @PostMapping("/admin/janitor/service/create")
    public String createJanitorServiceProcess(@Valid Service service, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
            model.addAttribute("serviceCategories", serviceCategoryRepository.findAll());
            return "admin/janitor/service/create";
        }

        serviceRepository.save(service);

        return "redirect:/admin/janitor/service";
    }
    @RequestMapping("/admin/janitor/service/detail")
    public String detailService(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("service", serviceRepository.findById(id).get());
        return "admin/janitor/service/detail";
    }

    @RequestMapping("/admin/janitor/service/edit")
    public String editService(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("serviceCategories", serviceCategoryRepository.findAll());
        model.addAttribute("service", serviceRepository.findById(id).get());
        return "admin/janitor/service/edit";
    }

    @PostMapping("/admin/janitor/service/edit")
    public String editAdminJanitorProcess(@Valid Service service, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
            model.addAttribute("serviceCategories", serviceCategoryRepository.findAll());

            return "admin/janitor/service/edit";
        }
        serviceRepository.save(service);

        return "redirect:/admin/janitor/service";
    }



    @RequestMapping("/admin/janitor/service/delete")
    public String deleteJanitorService(@RequestParam Integer id){
        Service service = serviceRepository.findById(id).get();
        serviceRepository.delete(service);
        return "redirect:/admin/janitor/service";
    }
}
