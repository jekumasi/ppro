package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.JanitorCategory;
import org.springframework.data.repository.CrudRepository;

public interface JanitorCategoryRepository extends CrudRepository<JanitorCategory, Integer> {
}
