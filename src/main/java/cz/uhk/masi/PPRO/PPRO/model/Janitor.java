package cz.uhk.masi.PPRO.PPRO.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

@Entity
public class Janitor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull(message = "Email je vyzadovan")
    @Length(max = 200)
    @Email
    @Column(unique = true)
    private String email;

    @NotNull(message = "Nazev je vyzadovan")
    @Length(max = 200)
    private String name;

    @NotNull(message = "Dosah sluzeb je vyzadovan")
    @Range(min = 1, max = 300, message = "Dosah sluzeb musi byt v rozmezi 1 - 300 km")
    private double serviceRange;

    private String description;

    @OneToOne
    private JanitorCategory category;

    @OneToOne
    private Address address;

    @OneToOne
    private JanitorUser janitorUser;

    @OneToMany(mappedBy = "janitor")
    private Set<Message> messages;

    @OneToMany(mappedBy = "janitor")
    private Set<Review> reviews;

    @OneToMany(mappedBy = "janitor")
    private Set<Service> services;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getServiceRange() {
        return serviceRange;
    }

    public void setServiceRange(double serviceRange) {
        this.serviceRange = serviceRange;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JanitorCategory getCategory() {
        return category;
    }

    public void setCategory(JanitorCategory category) {
        this.category = category;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public JanitorUser getJanitorUser() {
        return janitorUser;
    }

    public void setJanitorUser(JanitorUser janitorUser) {
        this.janitorUser = janitorUser;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }
}
