package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JanitorRepository extends CrudRepository<Janitor, Integer> {

//    @Query("SELECT j FROM Janitor j WHERE j.email = ':email'")
     List<Janitor> findByEmail(String email);

}
