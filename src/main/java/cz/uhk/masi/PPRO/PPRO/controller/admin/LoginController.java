package cz.uhk.masi.PPRO.PPRO.controller.admin;

import cz.uhk.masi.PPRO.PPRO.dao.*;
import cz.uhk.masi.PPRO.PPRO.model.Address;
import cz.uhk.masi.PPRO.PPRO.model.Customer;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.JanitorUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private JanitorCategoryRepository janitorCategoryRepository;
    @Autowired
    private JanitorRepository janitorRepository;
    @Autowired
    private JanitorUserRepository janitorUserRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(value = "/login")
    public String login(){
        return "login";
    }

    @RequestMapping(value = "/register/janitor")
    public String registerJanitorForm(Model model){
        Janitor janitor = new Janitor();
        janitor.setAddress(new Address());
        janitor.setJanitorUser(new JanitorUser());

        model.addAttribute("janitor", janitor);
        model.addAttribute("janitorCategories", janitorCategoryRepository.findAll());

        return "admin/register/janitor";
    }

    @PostMapping(value = "/register/janitor")
    public String registerJanitorSubmit(@Valid Janitor janitor, BindingResult bindingResultJanitor, Model model){

        model.addAttribute("janitorCategories", janitorCategoryRepository.findAll());
        if(bindingResultJanitor.hasErrors()){
            return "admin/register/janitor";
        }

        // encrypt password
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        janitor.getJanitorUser().setPassword(passwordEncoder.encode(janitor.getJanitorUser().getPassword()));

        // save entities
        addressRepository.save(janitor.getAddress());
        janitorUserRepository.save(janitor.getJanitorUser());
        janitorRepository.save(janitor);

        return "redirect:/admin";
    }

    @RequestMapping(value = "/register/customer")
    public String registerCustomerForm(Model model){
        Customer customer = new Customer();
        customer.setAddress(new Address());
        customer.setUser(new JanitorUser());

        model.addAttribute("customer", customer);

        return "admin/register/customer";
    }

    @PostMapping(value = "/register/customer")
    public String registerCustomerSubmit(@Valid Customer customer, BindingResult bindingResultCustomer){

        if(bindingResultCustomer.hasErrors()){
            return "admin/register/customer";
        }

        // encrypt password
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        customer.getUser().setPassword(passwordEncoder.encode(customer.getUser().getPassword()));

        // save entities
        addressRepository.save(customer.getAddress());
        janitorUserRepository.save(customer.getUser());
        customerRepository.save(customer);

        return "redirect:/admin";
    }

}
