package cz.uhk.masi.PPRO.PPRO.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull(message = "Nazev je vyzadovan")
    @Length(max = 100)
    private String name;

    @NotNull(message = "Popis je vyzadovan")
    @Length(max = 500)
    private String description;

    @NotNull(message = "Cena sluzby je vyzadovana")
    @Range(min = 1, max = 100000, message = "Cena sluzby musi byt v rozmezi 1 - 100 000 Kc")
    private double price;

    @OneToOne
    private ServiceCategory category;

    @ManyToOne
    private Janitor janitor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ServiceCategory getCategory() {
        return category;
    }

    public void setCategory(ServiceCategory category) {
        this.category = category;
    }

    public Janitor getJanitor() {
        return janitor;
    }

    public void setJanitor(Janitor janitor) {
        this.janitor = janitor;
    }
}
