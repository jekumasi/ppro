package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.Customer;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.Review;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReviewRepository extends CrudRepository<Review, Integer> {
    List<Review> findByJanitorId(int janitor_id);
    List<Review> findByCustomerId(int customer_id);

    @Query("SELECT r FROM Review r WHERE (r.janitor = ?2 AND r.customer= ?1) ORDER BY r.id")
    Review findByJanitorAndCustomer(Customer c, Janitor j);

    @Query("SELECT AVG(r.value) FROM Review r WHERE r.janitor = ?1")
    double getAvgByJanitor(Janitor janitor);
}
