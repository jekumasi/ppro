package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.JanitorRole;
import org.springframework.data.repository.CrudRepository;

public interface JanitorRoleRepository extends CrudRepository<JanitorRole, Integer> {
}
