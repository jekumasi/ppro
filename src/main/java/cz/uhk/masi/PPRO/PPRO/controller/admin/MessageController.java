package cz.uhk.masi.PPRO.PPRO.controller.admin;

import cz.uhk.masi.PPRO.PPRO.dao.CustomerRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorRepository;
import cz.uhk.masi.PPRO.PPRO.dao.MessageRepository;
import cz.uhk.masi.PPRO.PPRO.model.Customer;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.*;

@Controller
public class MessageController {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    JanitorRepository janitorRepository;
    @Autowired
    CustomerRepository customerRepository;

    @RequestMapping("/admin/admin/message")
    public String adminMessage(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("messages", GetAllCommunications());
        return "admin/admin/message/message";

    }

    @RequestMapping("/admin/admin/message/delete")
    public String adminMessageDelete(@RequestParam Integer id){

        Message message = messageRepository.findById(id).get();
        messageRepository.delete(message);

        return "redirect:/admin/admin/message";

    }

    @RequestMapping("/admin/janitor/message")
    public String janitorMessage(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        Janitor j = janitorRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);
        model.addAttribute("messages", GetAllCommunicationsJanitor(j));
        return "admin/admin/janitor/message";

    }

    @PostMapping("/admin/janitor/janitor-reply")
    public String janitorReply(@RequestParam Integer customerId, @RequestParam String messageContent, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        Janitor j = janitorRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);
        Customer c = customerRepository.findById(customerId).get();

        // build new message obj from request
        Message message = new Message();
        message.setFromJanitor(true);
        message.setCreated(new Date(System.currentTimeMillis()));
        message.setCustomer(c);
        message.setJanitor(j);
        message.setMessageContent(messageContent);
        // save it
        messageRepository.save(message);

        model.addAttribute("messages", GetAllCommunicationsJanitor(j));
        model.addAttribute("showCom", j.getName() + " | " + c.getName());

        return "admin/admin/janitor/message";

    }

    @RequestMapping("/admin/customer/message")
    public String customerMessage(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        Customer c = customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);
        model.addAttribute("messages", GetAllCommunicationsCustomer(c));
        return "admin/admin/customer/message";

    }

    @PostMapping("/admin/customer/customer-reply")
    public String customerReply(@RequestParam Integer janitorId, @RequestParam String messageContent, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        Janitor j = janitorRepository.findById(janitorId).get();
        Customer c = customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);

        // build new message obj from request
        Message message = new Message();
        message.setFromJanitor(false);
        message.setCreated(new Date(System.currentTimeMillis()));
        message.setCustomer(c);
        message.setJanitor(j);
        message.setMessageContent(messageContent);
        // save it
        messageRepository.save(message);

        model.addAttribute("messages", GetAllCommunicationsCustomer(c));
        model.addAttribute("showCom", j.getName() + " | " + c.getName());

        return "admin/admin/customer/message";

    }

    @RequestMapping("/admin/customer/message/create")
    public String customerMessageCreate(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        Customer c = customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);

        model.addAttribute("message", new Message());
        model.addAttribute("janitors", janitorRepository.findAll());

        return "admin/customer/message/create";

    }

    @PostMapping("/admin/customer/message/create")
    public String customerMessageCreateProcess(@Valid Message message, BindingResult bindingResult, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        if(bindingResult.hasErrors()){
            return "admin/customer/message/create";
        }

        Customer c = customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);

        message.setFromJanitor(false);
        message.setCustomer(c);
        message.setCreated(new Date(System.currentTimeMillis()));
        messageRepository.save(message);

        return "redirect:/admin/customer/message";

    }

    private Map<String, List<Message>> GetAllCommunications()
    {
        return processCommunications(messageRepository.findAll());
    }

    private Map<String, List<Message>> GetAllCommunicationsJanitor(Janitor j)
    {
        return processCommunications(messageRepository.findByJanitor(j));
    }

    private Map<String, List<Message>> GetAllCommunicationsCustomer(Customer c)
    {
        return processCommunications(messageRepository.findByCustomerId(c.getId()));
    }

    public Map<String, List<Message>> processCommunications(Iterable<Message> messages)
    {
        Map<String, List<Message>> communications = new HashMap<>();

        for(Message message : messages)
        {
            if (!communications.containsKey(message.getJanitor().getName() + " | " + message.getCustomer().getName()))
                communications.put(message.getJanitor().getName() + " | " + message.getCustomer().getName(), new ArrayList<>());

            List<Message> com = communications.get(message.getJanitor().getName() + " | " + message.getCustomer().getName());

            com.add(message);
        }

        return communications;
    }
}
