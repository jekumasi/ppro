package cz.uhk.masi.PPRO.PPRO.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull(message = "Zprava je vyzadovana")
    @Length(max = 500)
    private String messageContent;

    private boolean fromJanitor;

    @ManyToOne
    private Janitor janitor;

    @ManyToOne
    private Customer customer;

    private Date created;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public boolean isFromJanitor() {
        return fromJanitor;
    }

    public void setFromJanitor(boolean fromJanitor) {
        this.fromJanitor = fromJanitor;
    }

    public Janitor getJanitor() {
        return janitor;
    }

    public void setJanitor(Janitor janitor) {
        this.janitor = janitor;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    public static Message create() {
        return new Message();
    }
}
