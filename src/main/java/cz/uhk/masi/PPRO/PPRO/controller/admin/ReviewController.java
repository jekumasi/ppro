package cz.uhk.masi.PPRO.PPRO.controller.admin;


import cz.uhk.masi.PPRO.PPRO.dao.CustomerRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorRepository;
import cz.uhk.masi.PPRO.PPRO.dao.MessageRepository;
import cz.uhk.masi.PPRO.PPRO.dao.ReviewRepository;
import cz.uhk.masi.PPRO.PPRO.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@Controller
public class ReviewController {
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    JanitorRepository janitorRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    MessageRepository messageRepository;

    @RequestMapping("/admin/admin/review")
    public String adminReview(Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("reviews", reviewRepository.findAll());
        return "admin/admin/review/review";

    }

    @RequestMapping("/admin/janitor/review")
    public String janitorReview(Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        Janitor j = janitorRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);

        model.addAttribute("reviews", reviewRepository.findByJanitorId(j.getId()));
        if (reviewRepository.findByJanitorId(j.getId()).size() > 0)
            model.addAttribute("avg", reviewRepository.getAvgByJanitor(j));
        else
            model.addAttribute("avg", 0);
        return "admin/janitor/review";

    }

    @RequestMapping("/admin/customer/review")
    public String customerReview(Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        HashMap<Janitor, Review> reviews = new HashMap<>();
        Customer c = customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);

        List<Message> messages = messageRepository.findByCustomerId(c.getId());
        Review r = new Review();
        if(messages.size() > 0 && messages.get(0) != null)
        {
           for (Message m : messages) {
               r = reviewRepository.findByJanitorAndCustomer(m.getCustomer(), m.getJanitor());

               reviews.put(m.getJanitor(), r);

           }
        }

        model.addAttribute("reviews", reviews);
        return "admin/customer/review/review";

    }

    @RequestMapping("/admin/admin/review/detail")
    public String detailAdminReview(@RequestParam Integer id, Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("review", reviewRepository.findById(id).get());
        return "admin/admin/review/detail";
    }
    @RequestMapping("/admin/customer/review/create")
    public String createCustomerReview(@RequestParam Integer janitor_id, Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        Janitor j = janitorRepository.findById(janitor_id).get();
        Customer c = customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0);
        Review r = new Review();
        r.setJanitor(j);
        r.setCustomer(c);
        model.addAttribute("review",r);
        return "admin/customer/review/create";
    }
    @PostMapping("/admin/customer/review/create")
    public String createAdminReviewProcess(@Valid Review review, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

            return "admin/customer/review/create";
        }

        reviewRepository.save(review);

        return "redirect:/admin/customer/review";
    }

    @RequestMapping("/admin/admin/review/edit")
    public String editAdminReview(@RequestParam Integer id, Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("review", reviewRepository.findById(id).get());
        return "admin/admin/review/edit";
    }

    @RequestMapping("/admin/customer/review/edit")
    public String editCustomerReview(@RequestParam Integer id, Model model) {

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("review", reviewRepository.findById(id).get());
        return "admin/customer/review/edit";
    }

    @PostMapping("/admin/admin/review/edit")
    public String editAdminReviewProcess(@Valid Review review, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

            return "admin/admin/review/edit";
        }

        reviewRepository.save(review);

        return "redirect:/admin/admin/review/detail?id=" + review.getId();
    }
    @PostMapping("/admin/customer/review/edit")
    public String editCustomerReviewProcess(@Valid Review review, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

            return "admin/customer/review/edit";
        }

        reviewRepository.save(review);

        return "redirect:/admin/customer/review";
    }

    @RequestMapping("/admin/admin/review/delete")
    public String deleteAdminCustomer(@RequestParam Integer id) {
        Review review = reviewRepository.findById(id).get();
        reviewRepository.delete(review);

        return "redirect:/admin/admin/review";
    }
}
