package cz.uhk.masi.PPRO.PPRO.controller;

import cz.uhk.masi.PPRO.PPRO.dao.JanitorRepository;
import cz.uhk.masi.PPRO.PPRO.dao.ReviewRepository;
import cz.uhk.masi.PPRO.PPRO.dao.ServiceRepository;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
public class HomeController {

    @Autowired
    JanitorRepository janitorRepository;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    ServiceRepository serviceRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping(value = "/")
    public String index(Model model){
        List<Janitor> janitors = entityManager.createQuery("SELECT j FROM Janitor j", Janitor.class).setMaxResults(3).getResultList();
        int[] janitorRatings = new int[janitors.size()];
        for (int i = 0; i < janitors.size(); i++){
            try {
                // when 0 ratings in database -> null pointer exception
                janitorRatings[i] = (int) Math.round((Double) entityManager.createQuery("SELECT avg(r.value) AS value FROM Review r WHERE r.janitor.id = :janitorId").setParameter("janitorId", janitors.get(i).getId()).getSingleResult());
            } catch (Exception e){
                janitorRatings[i] = 0;
            }
        }

        model.addAttribute("authenticated", (SecurityContextHolder.getContext().getAuthentication().getPrincipal()) instanceof UserDetails);
        model.addAttribute("janitors", janitors);
        model.addAttribute("janitorRatings", janitorRatings);
        return "index";
    }

    @RequestMapping(value = "/janitors")
    public String janitors(Model model){
        List<Janitor> janitors = entityManager.createQuery("SELECT j FROM Janitor j", Janitor.class).getResultList();
        int[] janitorRatings = new int[janitors.size()];
        for (int i = 0; i < janitors.size(); i++){
            try {
                // when 0 ratings in database -> null pointer exception
                janitorRatings[i] = (int) Math.round((Double) entityManager.createQuery("SELECT avg(r.value) AS value FROM Review r WHERE r.janitor.id = :janitorId").setParameter("janitorId", janitors.get(i).getId()).getSingleResult());
            } catch (Exception e){
                janitorRatings[i] = 0;
            }
        }
        model.addAttribute("authenticated", (SecurityContextHolder.getContext().getAuthentication().getPrincipal()) instanceof UserDetails);
        model.addAttribute("janitors", janitors);
        model.addAttribute("janitorRatings", janitorRatings);
        return "home/janitors";
    }

    @RequestMapping(value = "/janitors/detail")
    public String janitorsDetail(@RequestParam Integer id, Model model){
        model.addAttribute("authenticated", (SecurityContextHolder.getContext().getAuthentication().getPrincipal()) instanceof UserDetails);
        Janitor j = janitorRepository.findById(id).get();
        double value = 0;
        if (reviewRepository.findByJanitorId(j.getId()).size() > 0)
            value = Math.round(reviewRepository.getAvgByJanitor(j));
        model.addAttribute("reviewValue", value);
        model.addAttribute("janitor", j);
        model.addAttribute("services", serviceRepository.findByJanitor(j));
        return "/home/janitor/detail";
    }

    @RequestMapping(value = "/contact")
    public String contact(Model model){
        model.addAttribute("authenticated", (SecurityContextHolder.getContext().getAuthentication().getPrincipal()) instanceof UserDetails);
        return "home/contact";
    }

}
