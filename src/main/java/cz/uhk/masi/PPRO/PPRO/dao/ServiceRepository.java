package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.Review;
import cz.uhk.masi.PPRO.PPRO.model.Service;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, Integer> {
    List<Service> findByJanitor(Janitor janitor);
}
