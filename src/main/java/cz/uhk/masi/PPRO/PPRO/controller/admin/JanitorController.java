package cz.uhk.masi.PPRO.PPRO.controller.admin;

import cz.uhk.masi.PPRO.PPRO.dao.AddressRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorCategoryRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorUserRepository;
import cz.uhk.masi.PPRO.PPRO.model.Address;
import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.JanitorUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class JanitorController {

    @Autowired
    JanitorRepository janitorRepository;
    @Autowired
    JanitorCategoryRepository janitorCategoryRepository;
    @Autowired
    JanitorUserRepository janitorUserRepository;
    @Autowired
    AddressRepository addressRepository;

    @RequestMapping("/admin/admin/janitor")
    public String adminJanitor(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("janitors", janitorRepository.findAll());
        return "admin/admin/janitor/janitor";

    }
    @RequestMapping("/admin/janitor/janitor")
    public String janitorJanitor(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
       model.addAttribute("janitor", janitorRepository.findByEmail(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0));

        return "admin/janitor/janitor";

    }

    @RequestMapping("/admin/admin/janitor/detail")
    public String detailAdminJanitor(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("janitor", janitorRepository.findById(id).get());
        return "admin/admin/janitor/detail";
    }

    @RequestMapping("/admin/admin/janitor/edit")
    public String editAdminJanitor(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("janitorCategories", janitorCategoryRepository.findAll());
        model.addAttribute("janitor", janitorRepository.findById(id).get());
        return "admin/admin/janitor/edit";
    }
    @RequestMapping("/admin/janitor/edit")
    public String editJanitor(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("janitorCategories", janitorCategoryRepository.findAll());
        model.addAttribute("janitor", janitorRepository.findById(id).get());
        return "admin/janitor/edit";
    }

    @PostMapping("/admin/admin/janitor/edit")
    public String editAdminJanitorProcess(@Valid Janitor janitor, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
            model.addAttribute("janitorCategories", janitorCategoryRepository.findAll());

            return "admin/admin/janitor/edit";
        }

        addressRepository.save(janitor.getAddress());
        janitorRepository.save(janitor);

        return "redirect:/admin/admin/janitor/detail?id=" + janitor.getId();
    }
    @PostMapping("/admin/janitor/edit")
    public String editJanitorProcess(@Valid Janitor janitor, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
            model.addAttribute("janitorCategories", janitorCategoryRepository.findAll());

            return "admin/janitor/edit";
        }

        addressRepository.save(janitor.getAddress());
        janitorRepository.save(janitor);

        return "redirect:/admin/janitor/janitor?id=" + janitor.getId();
    }

    @RequestMapping("/admin/admin/janitor/delete")
    public String deleteAdminJanitor(@RequestParam Integer id){
        Janitor janitor = janitorRepository.findById(id).get();
        Address address = janitor.getAddress();
        JanitorUser janitorUser = janitor.getJanitorUser();

        janitorRepository.delete(janitor);
        addressRepository.delete(address);
        janitorUserRepository.delete(janitorUser);

        return "redirect:/admin/admin/janitor";
    }

}
