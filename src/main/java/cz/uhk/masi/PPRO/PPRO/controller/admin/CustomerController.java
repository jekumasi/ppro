package cz.uhk.masi.PPRO.PPRO.controller.admin;

import cz.uhk.masi.PPRO.PPRO.dao.AddressRepository;
import cz.uhk.masi.PPRO.PPRO.dao.CustomerRepository;
import cz.uhk.masi.PPRO.PPRO.dao.JanitorUserRepository;
import cz.uhk.masi.PPRO.PPRO.model.Address;
import cz.uhk.masi.PPRO.PPRO.model.Customer;
import cz.uhk.masi.PPRO.PPRO.model.JanitorUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    JanitorUserRepository janitorUserRepository;
    @Autowired
    AddressRepository addressRepository;

    @RequestMapping("/admin/admin/customer")
    public String adminCustomer(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("customers", customerRepository.findAll());
        return "admin/admin/customer/customer";

    }
    @RequestMapping("/admin/customer/customer")
    public String customerCustomer(Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("customer", customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0));

        return "admin/customer/customer";

    }
    @RequestMapping("/admin/admin/customer/detail")
    public String detailAdminCustomer(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        model.addAttribute("customer", customerRepository.findById(id).get());
        return "admin/admin/customer/detail";
    }
    @RequestMapping("/admin/customer/edit")
    public String editCustomerCustomer(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("customer", customerRepository.findByName(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get(0));

        return "admin/customer/edit";
    }
    @PostMapping("/admin/customer/edit")
    public String editCustomerCustomerProcess(@Valid Customer customer, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

            return "admin/customer/edit";
        }

        addressRepository.save(customer.getAddress());
        customerRepository.save(customer);

        return "redirect:/admin/customer/customer";
    }

    @RequestMapping("/admin/admin/customer/edit")
    public String editAdminCustomer(@RequestParam Integer id, Model model){

        // authentication
        model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("customer", customerRepository.findById(id).get());
        return "admin/admin/customer/edit";
    }
    @PostMapping("/admin/admin/customer/edit")
    public String editAdminCustomerProcess(@Valid Customer customer, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            // authentication
            model.addAttribute("userRole", SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
            model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

            return "admin/admin/customer/edit";
        }

        addressRepository.save(customer.getAddress());
        customerRepository.save(customer);

        return "redirect:/admin/admin/customer/detail?id=" + customer.getId();
    }
    @RequestMapping("/admin/admin/customer/delete")
    public String deleteAdminCustomer(@RequestParam Integer id){
        Customer customer = customerRepository.findById(id).get();
        Address address = customer.getAddress();
        JanitorUser janitorUser = customer.getUser();

        customerRepository.delete(customer);
        addressRepository.delete(address);
        janitorUserRepository.delete(janitorUser);

        return "redirect:/admin/admin/janitor";
    }

}
