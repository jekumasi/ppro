package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.JanitorUser;
import org.springframework.data.repository.CrudRepository;

public interface JanitorUserRepository extends CrudRepository<JanitorUser, Integer> {
}
