package cz.uhk.masi.PPRO.PPRO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                    .antMatchers("/", "/home", "/janitors", "/janitors/**", "/janitor", "/janitor/**", "/contact", "/register/**").permitAll()
                    .antMatchers("/css/**", "/images/**", "/scripts/**", "/static/**").permitAll()
                    .antMatchers("/admin/admin/**").hasAuthority("admin")
                    .antMatchers("/admin/janitor/**").hasAuthority("janitor")
                    .antMatchers("/admin/customer/**").hasAuthority("customer")
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .and()
                .logout().logoutUrl("/login?logout")
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                    .permitAll();

        http.csrf().disable();
        http.headers().frameOptions().sameOrigin();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("SELECT login AS USERNAME, password AS PASSWORD, 1 as enabled FROM janitor_user WHERE login = ?")
                .authoritiesByUsernameQuery("SELECT login AS USERNAME, janitor_role.identificator AS ROLE FROM janitor_user INNER JOIN janitor_role ON janitor_user.role_id = janitor_role.id WHERE login = ?");
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return  new BCryptPasswordEncoder();
    }

    /*@Bean
    @Override
    public UserDetailsService userDetailsService(){
        UserDetails user =
                User.withDefaultPasswordEncoder()
                .username("user")
                .password("password")
                .roles("USER")
                .build();

        return new InMemoryUserDetailsManager(user);
    }*/

}
