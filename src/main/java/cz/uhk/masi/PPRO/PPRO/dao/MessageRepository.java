package cz.uhk.masi.PPRO.PPRO.dao;

import cz.uhk.masi.PPRO.PPRO.model.Janitor;
import cz.uhk.masi.PPRO.PPRO.model.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface MessageRepository extends CrudRepository<Message, Integer> {

    List<Message> findByCustomerId(int customer_id);
    List<Message> findByJanitor(Janitor j);

}
